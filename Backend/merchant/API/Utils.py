"""Author: Chirag and Deepanshu"""
import requests
import json
import datetime
import os
#from .Codes import Codes
import pickle


def get_realtime_data(self,payload):
    #need to generate unique message id below
        # TODO: TRY CATCH HERE
    print(payload)
    request_data_json = requests.post(self.url,cert=(self.cert,self.key),headers = self.headers,auth = (self.userid,self.password),json = str(payload),timeout = 10).json()
    return request_data_json


def get_name(self,payload):
    with open('./local_data/file_index.txt') as myfile:
        for line in myfile:
            if(line[0]=='#'):
                json_r=line[1:]
            else:
                filename=line
                if(json_r==str(payload)):
                    return filename

    return "NIL"


def search_database(self,payload):
    name=self.get_name(payload);
    if(name=="NIL"):
        return "NIL"
    dr=pickle.load(open(name,"rb"))
    return dr

def get_data(self,payload,date):
    res1 = self.search_database(payload)
    if(res1!='NIL'):
            return res1
        res2 = self.get_realtime_data(payload)
        if(res2!='NIL'):
            pickle.dump(res2,open('./local_data/'+date+'.p',"wb"))
            with open("./local_data/file_index.txt", "a") as myfile:
                myfile.write("#"+str(payload))
                myfile.write(date+'.p')
            return res2
        return 'NIL'

    def states_general_request():
        pass


    def filtered_response(params):
        pass


    def comparitive_response(params):
        pass


#     def getstatelist(self):
#         cd=Codes()
#         return cd.countrySubdivisionList
    def getprevmonth(self,s):
        year=int(s[:4])
        month=int(s[4:])
        if month==1:
            month=12
            year-=1
        else:
            month-=1
        return str(year)+str(month)
    def past_n_months(self,n,curr_mon):
        ld=[]
        for i in range(n):
            curr_mon= self.getprevmonth(curr_mon)
            date = datetime.datetime.now().strftime('%Y-%m-%d%T%H:%M:%S')
            uniqueId = "6da60e1b8b024532a2e0eacb1af58581"
            payload = json.JSONEncoder().encode({
                "requestHeader": {"messageDateTime": date, "requestMessageId": uniqueId},
                "requestData": {
                    "naicsCodeList": [""],
                    "merchantCategoryCodeList": ["5812"],
                    "merchantCategoryGroupsCodeList": [""],
                    "postalCodeList": [""],
                    "msaList":[""],
                    "countrySubdivisionList":[""],
                    "merchantCountry":"840",
                    "monthList": [curr_mon],
                    "accountFundingSourceList":["ALL"],
                    "eciIndicatorList":["ALL"],
                    "platformIDList": ["ALL"],
                    "posEntryModeList":["ALL"],
                    "cardPresentIndicator": "CARDPRESENT",
                    "groupList": ["STANDARD"]
                }
            })

            dt=getdata(payload)
            return dt



payload = json.loads(''' 

{
"requestHeader": {
"messageDateTime": "2020-06-22T03:17:44.327Z",
"requestMessageId": "6da60e1b8b024532a2e0eacb1af58581"
},
"requestData": {
"naicsCodeList": [
""
],
"merchantCategoryCodeList": [
"5812"
],
"merchantCategoryGroupsCodeList": [
""
],
"postalCodeList": [
""
],
"msaList": [
"5602"
],
"countrySubdivisionList": [
""
],
"merchantCountry": "840",
"monthList": [
"201706"
],
"accountFundingSourceList": [
"ALl"
],
"eciIndicatorList": [
"All"
],
"platformIDList": [
"All"
],
"posEntryModeList": [
"All"
],
"cardPresentIndicator": "CARDPRESENT",
"groupList": [
"STANDARD"
]
}
}''')

headers = {"Accept": "application/json"}
body = {}
print(payload)
dr=Utils()
print(dr.get_realtime_data(payload))
#r = requests.post(url, cert=(cert, key), headers=headers, auth=(userid, password), json=payload, timeout=10).json()
#print(r)



