// ***********************
// DRAWING THE CHARTS
// ***********************

var data = parsed_data.Data;

// ***********************
// REVENUE CHART
// ***********************

var revenueObj = {
	title: 'Revenue Trend',
	json: {
		data1: data['RevenueLoss Abs']
	},
	names: {
		data1: 'Revenue Loss'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_h1'
};

lineChart.draw(revenueObj);

revenueObj.id = '#areaSpline_h1';
areaChart.draw(revenueObj);

revenueObj.id = '#barChart_h1';
revenueObj.json.data1 = data['RevenueLoss Rel'];
barChart.draw(revenueObj);

revenueObj.id = '#bubbleChart_h1';
bubbleChart.draw(revenueObj);



// ********************************
// AVERAGE CARDHOLDER SPENT CHART
// ********************************

var cardholderObj = {
	title: 'Average Cardholder Spent',
	json: {
		data1: data['AverageCardHolderSpent Abs']
	},
	names: {
		data1: 'Spent'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_h2'
};

lineChart.draw(cardholderObj);

cardholderObj.id = '#areaSpline_h2';
areaChart.draw(cardholderObj);

cardholderObj.id = '#barChart_h2';
cardholderObj.json.data1 = data['AverageCardHolderSpent Rel'];

barChart.draw(cardholderObj);


cardholderObj.id = '#bubbleChart_h2';
bubbleChart.draw(cardholderObj);



// ************************************
// AVERAGE TRANSACTION FREQUENCY CHART
// ************************************	

var transactionObj = {
	title: 'Average Transaction Frequency',
	json: {
		data1: data['CardTransactionFrequency Abs']
	},
	names: {
		data1: 'Frequency'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_h3'
};

lineChart.draw(transactionObj);


transactionObj.id = '#areaSpline_h3';
areaChart.draw(transactionObj);

transactionObj.id = '#barChart_h3';
transactionObj.json.data1 = data['CardTransactionFrequency Rel'];

barChart.draw(transactionObj);

transactionObj.id = '#bubbleChart_h3';
bubbleChart.draw(transactionObj);



// *************************
// UNEMPLOYEMENT RATE CHART
// *************************

var unemployementObj = {
	title: 'Unemployement Rate',
	json: {},
	names: {
		data1: 'Rate'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: ''
};


var data_values = [
	"Unemployment Rate - 20 Years & Over Men",
	"Unemployment Rate - 20 Years & Over Women",
	"Unemployment Rate - 16-19 Years",
	"Unemployment Rate - White",
	"Unemployment Rate - Black or African American",
	"Unemployment Rate - Asian",
	"Unemployment Rate - Hispanic or Latino",
	"Unemployment Rate - 25 Years & Over,  Less than a High School Diploma",
	"Unemployment Rate - 25 Years & Over,  High School Graduates No College",
	"Unemployment Rate - 25 Years & Over,  Some College or Associate Degree",
	"Unemployment Rate - 25 Years & Over,  Bachelor's Degree and Higher"
];

var count = 4;

for (var i = 0; i < data_values.length; i++) {

	unemployementObj.json.data1 = data[data_values[i] + ' Abs'];
	
	unemployementObj.id = '#lineChart_h' + count;
	lineChart.draw(unemployementObj);

	unemployementObj.id = '#areaSpline_h' + count;
	areaChart.draw(unemployementObj);

	unemployementObj.json.data1 = data[data_values[i] + ' Rel'];

	unemployementObj.id = '#barChart_h' + count;
	barChart.draw(unemployementObj);

	unemployementObj.id = '#bubbleChart_h' + count;
	bubbleChart.draw(unemployementObj);

	count = count + 1;
}



// ************************************
// E-COMM AND NON E-COMM SPENT CHART
// ************************************	

var commerceObj = {
	title: 'E-Comm & Non E-Comm Spent',
	json: {
		data1: data['EcommerceSpent Abs'],
		data2: data['NonEcommerceSpent Abs']
	},
	names: {
		data1: 'E-Comm',
		data2: 'Non E-Comm'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_h15'
};

lineChart.draw(commerceObj);

commerceObj.id = '#areaSpline_h15';
areaChart.draw(commerceObj);

commerceObj.id = '#barChart_h15';
commerceObj.json.data1 = data['EcommerceSpent Rel'];
commerceObj.json.data2 = data['NonEcommerceSpent Rel'];

barChart.draw(commerceObj);


commerceObj.id = '#bubbleChart_h15';
bubbleChart.draw(commerceObj);



// ************************************
// TOP 5 HIT CATEGORIES CHART
// ************************************	

var categoryObj = {
	title: 'Top 5 Hit Categories',
	json: {
		data1: data['Halls Abs'],
		data2: data['Casinos Abs'],
		data3: data['Charity Abs'],
		data4: data['Travel Abs'],
		data5: data['MassMediaBooks Abs']
	},
	names: {
		data1: 'Halls',
		data2: 'Casinos',
		data3: 'Charity',
		data4: 'Travel',
		data5: 'Mass Media Books'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_h16'
};

lineChart.draw(categoryObj);

categoryObj.id = '#areaSpline_h16';
areaChart.draw(categoryObj);

categoryObj.id = '#barChart_h16';
categoryObj.json.data1 = data['Halls Rel'];
categoryObj.json.data2 = data['Casinos Rel'];
categoryObj.json.data3 = data['Charity Rel'];
categoryObj.json.data4 = data['Travel Rel'];
categoryObj.json.data5 = data['MassMediaBooks Rel'];

barChart.draw(categoryObj);


categoryObj.id = '#bubbleChart_h16';
bubbleChart.draw(categoryObj);



// toggle functions

function showDiv1(l,a){
	document.getElementById(l).style.display="block";
	document.getElementById(a).style.display="none";

}	

function showDiv2(l,a){
	document.getElementById(l).style.display="none";
	document.getElementById(a).style.display="block";

}	

function showDiv3(bar,bub){
	document.getElementById(bar).style.display="block";
	document.getElementById(bub).style.display="none";

}	

function showDiv4(bar,bub){
	document.getElementById(bar).style.display="none";
	document.getElementById(bub).style.display="block";

}