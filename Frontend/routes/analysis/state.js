var express = require('express'),
	router 	= express.Router({mergeParams: true}),
	fetch 	= require('node-fetch'),
	sample	= require('../../data/sample');



// ROUTE FOR SHOWING STATE ANALYSIS
router.get('/', (req, res) => {

	var state_code = req.query.state_code;

	fetch(`http://localhost:8000/merchant/states/data?statecode=${state_code}`)
		.then((res) => res.json())
		.then((json) => {
			res.render('analysis/state', {
				title: `${state_code} Analysis`,
				data: json
			});
		})
		.catch((err) => {
			console.log(err.message);
			res.render('error/timedOut', {
				title: 'Request Timed Out'
			});
		});
});


module.exports = router;