// ***********************
// DRAWING THE CHARTS
// ***********************

var data = parsed_data.Data;


// ***********************
// REVENUE CHART
// ***********************

var revenueObj = {
	title: 'Revenue Trend',
	json: {
		data1: data.RevenueLoss
	},
	names: {
		data1: 'Revenue Loss'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_m1'
};

lineChart.draw(revenueObj);

revenueObj.id = '#barChart_m1';
barChart.draw(revenueObj);

revenueObj.id = '#areaSpline_m1';
areaChart.draw(revenueObj);



// ********************************
// AVERAGE CARDHOLDER SPENT CHART
// ********************************

var cardholderObj = {
	title: 'Average Cardholder Spent',
	json: {
		data1: data.AverageCardHolderSpent
	},
	names: {
		data1: 'Spent'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_m2'
};

lineChart.draw(cardholderObj);

cardholderObj.id = '#barChart_m2';
barChart.draw(cardholderObj);

cardholderObj.id = '#areaSpline_m2';
areaChart.draw(cardholderObj);



// ************************************
// AVERAGE TRANSACTION FREQUENCY CHART
// ************************************	

var transactionObj = {
	title: 'Average Transaction Frequency',
	json: {
		data1: data.CardTransactionFrequency
	},
	names: {
		data1: 'Frequency'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_m3'
};

lineChart.draw(transactionObj);

transactionObj.id = '#barChart_m3';
barChart.draw(transactionObj);

transactionObj.id = '#areaSpline_m3';
areaChart.draw(transactionObj);



// *************************
// UNEMPLOYEMENT RATE CHART
// *************************

var unemployementObj = {
	title: 'Unemployement Rate',
	json: {
		data1: data.UnemploymentRate || data['Unemployement Rate']
	},
	names: {
		data1: 'Rate'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_m4'
};

lineChart.draw(unemployementObj);

unemployementObj.id = '#barChart_m4';
barChart.draw(unemployementObj);

unemployementObj.id = '#areaSpline_m4';
areaChart.draw(unemployementObj);



// ************************************
// E-COMM AND NON E-COMM SPENT CHART
// ************************************	

var commerceObj = {
	title: 'E-Comm & Non E-Comm Spent',
	json: {
		data1: data.EcommerceSpent,
		data2: data.NonEcommerceSpent
	},
	names: {
		data1: 'E-Comm',
		data2: 'Non E-Comm'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_m5'
};

lineChart.draw(commerceObj);

commerceObj.id = '#barChart_m5';
barChart.draw(commerceObj);

commerceObj.id = '#areaSpline_m5';
areaChart.draw(commerceObj);



// ************************************
// TOP 5 HIT CATEGORIES CHART
// ************************************	

var categoryObj = {
	title: 'Top 5 Hit Categories',
	json: {
		data1: data.Halls,
		data2: data.Casinos,
		data3: data.Charity,
		data4: data.Travel,
		data5: data.MassMediaBooks
	},
	names: {
		data1: 'Halls',
		data2: 'Casinos',
		data3: 'Charity',
		data4: 'Travel',
		data5: 'Mass Media Books'
	},
	colors: {

	},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: '#lineChart_m6'
};

lineChart.draw(categoryObj);

categoryObj.id = '#barChart_m6';
barChart.draw(categoryObj);

categoryObj.id = '#areaSpline_m6';
areaChart.draw(categoryObj);


// toggle functions

function showDiv1(l,b,a){
	document.getElementById(l).style.display="block";
	document.getElementById(a).style.display="none";
	document.getElementById(b).style.display="none";

}	

function showDiv2(l,b,a){
	document.getElementById(l).style.display="none";
	document.getElementById(a).style.display="block";
	document.getElementById(b).style.display="none";

}	

function showDiv3(l,b,a){
	document.getElementById(l).style.display="none";
	document.getElementById(a).style.display="none";
	document.getElementById(b).style.display="block";

}	