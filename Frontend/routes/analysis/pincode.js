var express = require('express'),
	router 	= express.Router({mergeParams: true}),
	fetch 	= require('node-fetch'),
	sample	= require('../../data/sample');



// ROUTE FOR SHOWING PINCODE ANALYSIS
router.get('/', (req, res) => {

	var pin_code = req.query.pin_code;

	fetch(`http://localhost:8000/merchant/pincodes/data?pincode=${pin_code}`)
		.then((res) => res.json())
		.then((json) => {
			res.render('analysis/pincode', {
				title: `Pincode - ${pin_code} Analysis`,
				data: json
			});
		})
		.catch((err) => {
			console.log(err.message);
			res.render('error/timedOut', {
				title: 'Request Timed Out'
			});
		});
});



module.exports = router;