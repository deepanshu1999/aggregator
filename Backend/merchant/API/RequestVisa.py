"""Author: Deepanshu Aggarwal"""
import requests
import json
import os
import pickle
CREDENTIALS_LOAD_ERROR=1
SERVER_CONNECTION_FAILURE=2
def get_data(payload):
	# Level 1 function to connect to Cloud and Get response
	try:
		dic=pickle.load(open(os.getcwd()+'/merchant/credentials/ID.p','rb'))
		url=dic["url"]
		cert=dic["cert"]
		key=dic["key"]
		userid=dic["userid"]
		password=dic["password"]
		headers=dic["headers"]
	except:
		return CREDENTIALS_LOAD_ERROR
	try:
		r = requests.post(url,cert=(os.getcwd()+'/merchant/credentials/'+cert,os.getcwd()+'/merchant/credentials/'+key),headers = headers,auth = (userid,password),json = payload,timeout = 10).json()
		return r
	except:
		return SERVER_CONNECTION_FAILURE

#-------TESTING CODE -------#
# payload = json.loads( ''' 
 
# {
# "requestHeader": {
# "messageDateTime": "2020-06-22T03:17:44.327Z",
# "requestMessageId": "6da60e1b8b024532a2e0eacb1af58581"
# },
# "requestData": {
# "naicsCodeList": [
# ""
# ],
# "merchantCategoryCodeList": [
# "5812"
# ],
# "merchantCategoryGroupsCodeList": [
# ""
# ],
# "postalCodeList": [
# ""
# ],
# "msaList": [
# "7362"
# ],
# "countrySubdivisionList": [
# ""
# ],
# "merchantCountry": "840",
# "monthList": [
# "201706"
# ],
# "accountFundingSourceList": [
# "ALl"
# ],
# "eciIndicatorList": [
# "All"
# ],
# "platformIDList": [
# "All"
# ],
# "posEntryModeList": [
# "All"
# ],
# "cardPresentIndicator": "CARDPRESENT",
# "groupList": [
# "STANDARD"
# ]
# }
# }''')
# print(get_data(payload))