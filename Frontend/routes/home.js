var express = require('express'),
	router 	= express.Router({mergeParams: true}),
	fetch 	= require('node-fetch'),
	sample	= require('../data/sample');



// ROUTE FOR HOME PAGE
router.get('/', (req, res) => {

	var country_code = 840;

	fetch(`http://localhost:8000/merchant/countries/data?countrycode=${country_code}`)
		.then((res) => res.json())
		.then((json) => {
			res.render('home', {
				title: 'Home',
				data: json
			});
		})
		.catch(err => {
			console.log(err.message);
			res.render('error/timedOut', {
				title: 'Request Timed Out'
			});
		});
});



module.exports = router;