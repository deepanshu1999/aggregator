from django.urls import path
from . import views

urlpatterns = [
	
	#path('countries/list',views.getcountrylist),
	path('countries/data',views.getcountrydata),

    path('states/list', views.getstatelist),
    path('states/data',views.getstatedata),

    path('pincodes/list',views.getpincodelist),
    path('pincodes/data',views.getpincodedata),

    path('msa/list',views.getmsalist),
    path('msa/data',views.getmsadata),

    path('compare',views.compare)

]