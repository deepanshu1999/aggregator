"""Author : Chirag Jasuja"""
import json
import datetime
from dateutil.relativedelta import relativedelta


def update_top_hit_categories(dict_data):
    change_dict = {}
    for key in dict_data.keys():
        if key.replace(" Rel", "") in ["GroceriesSuperMarket", "Transportation", "Cabs", "ShoesClothes", "FoodDrink",
                                       "Electronics", "Stationary", "Education", "Hardware", "Automobile", "Medical",
                                       "Religious", "Petroleum", "Telecommunication", "Travel", "Construction",
                                       "MassMediaBooks", "Mobile home dealer", "SalvageRepairs", "Service", "Halls",
                                       "Sports", "Casinos", "Charity"]:
            change_dict[key] = yearly_change(dict_data[key])
    sorted_change = sorted(change_dict.keys(), key=lambda x: x[1])
    dict_data["Top5HitCategories"] = sorted_change[0:5]
    return dict_data



def yearly_change(property_array):
    current = 100
    if len(property_array) < 11:
        return None
    for change in property_array[-11:]:
        current += current * change
    total_change = current - 100
    total_change = round(total_change, 2)
    return total_change


def get_year(array, last_updated, to_find):
    index = 0
    if to_find == "max":
        max_val = -100
        for i in range(12):
            if max_val < array[-i]:
                index = i
                max_val = array[-i]
    else:
        min_val = 100
        for i in range(12):
            if min_val > array[-i]:
                index = i
                min_val = array[-i]
    last_updated = datetime.datetime.strptime(last_updated, "%Y-%m-%d %H:%M%S")
    req_time = last_updated + relativedelta(months=-index)
    return datetime.datetime.strftime(req_time, "%B,%Y")


def string_gen(property_name, property_array, parent_change, parent_type, last_updated):
    update_property_name = {"groceriessupermarket": " from Groceries items and supermarkets",
                            "transportation": " from transportation", "cabs": " from cabs",
                            "shoesclothes": " from Clothiing and footwear",
                            "fooddrink": " from food and drinking oultlets", "electronics": " from Electronic items",
                            "stationary": " from Stationary items", "education": " from Education",
                            "hardware": " from Hardware related shops", "automobile": " from Automobile sales",
                            "medical": " from Hospitals and chemist shops", "religious": " from religious goods",
                            "petroleum": " from Petroleum and its products", "telecommunication": " from Communication",
                            "travel": " from Travel related goods", "construction": " from Construction",
                            "massmediabooks": " from Mass Media and Book stores",
                            "mobile home dealer": " from Mobile home dealers",
                            "salvagerepairs": " from Salvage and repair shops", "service": " from Services",
                            "halls": " from Banquet halls", "sports": " from Sports related businesses",
                            "casinos": " from Casinos", "charity": " from Charities", "revenueloss": ""}
    change = yearly_change(property_array)
    property_name = property_name.replace(" Rel", "")
    property_name = update_property_name.get(property_name.lower(), property_name)
    #   increase in revenue
    string_1 = ""
    string_2 = ""
    if change > 0:
        string_1 = "Revenue collection{} increased by {}% as compared to last year.".format(property_name, change)
        date_year = get_year(property_array, last_updated, "max")
        string_2 = "Max revenue collection occurred in {}".format(date_year)

    if change < 0:
        string_1 = "Revenue collection{} decreased by {}% as compared to last year.".format(property_name, abs(change))
        date_year = get_year(property_array, last_updated, "min")
        string_2 = "Max revenue loss occurred in {}".format(date_year)
    if parent_type is not None and parent_change != 0:
        string_3 = "The revenue collection{} changed by ratio of {} as compared to {}.".format(property_name,round(change/parent_change,2),parent_type)
        return [string_1, string_2, string_3]
    return [string_1, string_2]


def add_insights(data, parent_data = None):
    '''data : json format or dictionary , insights are appended to this data and returned as a dictionary
        parent_data: json or dictionary format, used to compare with the data, If no parent then pass None'''
    if type(data) == 'str':
        dict_data = json.loads(data)
    else :
        dict_data = data

    if type(parent_data) == 'str':
        parent_dict = json.loads(parent_data)
    else:
        parent_dict = parent_data
    if parent_data is not None:
        parent_type = ""
        if parent_dict.get("StateName", "a") != "a":
            parent_type = "State"
        else:
            if parent_dict.get("Country", "a") != "a":
                parent_type = "Country"
            else:
                if parent_dict.get("MSAName", "a") != "a":
                    parent_type = "City"
                else:
                    parent_type = "District"
    else:
        parent_dict = None
        parent_type = None
    dict_data = update_top_hit_categories(dict_data)

    # yahan par time ki key daal dena
    # last_updated = dict_data[""]
    last_updated = datetime.datetime.now().strftime("%Y-%m-%d %H:%M%S")

    insights = {}
    req_keys = dict_data.get("Top5HitCategories", []).copy()
    req_keys.append("RevenueLoss")
    for key in req_keys:
        try:
            parent_yearly_change = yearly_change(parent_dict[key])
        except KeyError:
            parent_yearly_change = yearly_change(parent_dict[key + " Rel"])
        except TypeError:
            parent_yearly_change = 0
        try:
            a = dict_data[key]
        except KeyError:
            a = dict_data[key + ' Rel']
            pass
        str = string_gen(key, a, parent_yearly_change, parent_type, last_updated)
        insights[key] = str
    dict_data["insights"] = insights

    return dict_data


# json_v = json.dumps({"_id": 10001, "Population": 28969,
#                      "RevenueLoss": [-0.04, 0.11, -0.09, -0.18, -0.02, 0.01, -0.1, 0.06, -0.17, -0.02, -0.06, 0.13,
#                                      0.14, 0.07, 0.1, 0.13, 0.08, 0.17, -0.07, 0.19, 0.03, -0.07, -0.22, 0.01],
#                      "GroceriesSuperMarket": [-0.14, -0.08, -0.09, 0.11, 0.13, -0.14, 0.07, 0.01, 0.13, -0.24, -0.08,
#                                               0.19, -0.05, 0.21, -0.16, -0.1, -0.14, -0.11, -0.08, 0.08, 0.24, -0.09,
#                                               -0.14, 0.38],
#                      "Transportation": [-0.06, 0.16, 0.05, -0.12, 0.05, -0.15, -0.02, 0.07, 0.06, -0.25, -0.14, 0.04,
#                                         0.03, 0.04, 0.04, -0.1, 0.04, 0.0, 0.1, 0.11, 0.15, -0.12, -0.04, 0.11],
#                      "Cabs": [0.08, 0.01, -0.06, 0.13, 0.01, 0.06, 0.2, 0.06, 0.11, -0.21, 0.04, 0.19, 0.06, 0.18, 0.05,
#                               0.03, -0.08, -0.09, -0.09, 0.09, -0.04, -0.28, -0.17, 0.05],
#                      "ShoesClothes": [0.13, 0.04, 0.08, -0.14, 0.08, 0.01, 0.17, 0.16, 0.29, -0.54, 0.1, 0.05, -0.1,
#                                       0.09, 0.08, 0.08, 0.21, -0.19, 0.12, 0.05, 0.33, -0.31, -0.12, 0.04],
#                      "FoodDrink": [0.06, -0.18, 0.23, -0.11, 0.09, 0.0, -0.1, 0.14, -0.13, 0.04, -0.22, 0.06, 0.09,
#                                    -0.15, 0.2, 0.02, 0.17, -0.06, 0.02, -0.05, 0.12, -0.01, 0.0, -0.04, 0.11],
#                      "Electronics": [0.03, 0.25, -0.04, 0.14, -0.06, -0.04, 0.09, -0.1, -0.15, 0.02, 0.02, 0.27, -0.09,
#                                      0.19, 0.02, 0.12, 0.05, -0.23, 0.21, -0.03, -0.0, -0.28, -0.0, -0.06],
#                      "Stationaries": [3.1, 3.2, 3.5, 2.0, -0.5, -1, -1.5, -1.5, 0, 3, 4, 4.3],
#                      "Education": [-0.15, 0.09, 0.09, 0.14, 0.05, -0.25, 0.2, -0.01, 0.07, -0.1, 0.03, 0.18, -0.14,
#                                    -0.1, -0.17, 0.06, -0.15, -0.21, -0.07, -0.06, 0.19, -0.08, 0.08, -0.08],
#                      "Hardware": [-0.02, 0.28, -0.0, -0.02, 0.07, -0.08, 0.02, 0.25, 0.13, -0.53, 0.16, 0.11, -0.01,
#                                   0.08, 0.09, -0.1, 0.05, -0.1, 0.24, 0.23, 0.22, -0.52, -0.03, -0.08],
#                      "Automobile": [0.1, 0.12, -0.14, 0.03, 0.17, 0.01, 0.12, -0.05, 0.04, -0.3, 0.17, 0.13, -0.17,
#                                     -0.06, -0.01, -0.05, 0.03, -0.06, 0.06, 0.06, 0.04, -0.17, 0.16, -0.14],
#                      "Medical": [-0.02, 0.2, 0.05, 0.02, 0.03, -0.23, 0.02, 0.13, -0.05, 0.03, 0.04, 0.02, 0.16, -0.02,
#                                  -0.07, 0.18, -0.09, -0.19, 0.07, 0.01, 0.0, -0.04, -0.0, 0.11],
#                      "Religious": [-0.07, 0.13, -0.16, 0.01, 0.12, 0.03, -0.09, -0.11, 0.16, -0.26, -0.23, 0.27, -0.02,
#                                    0.16, -0.19, -0.12, 0.04, -0.1, 0.2, 0.15, 0.14, -0.26, -0.12, 0.38],
#                      "Petroleum": [0.12, 0.03, -0.17, 0.03, -0.06, -0.08, 0.2, -0.17, 0.04, -0.04, 0.07, 0.24, 0.01,
#                                    0.18, -0.12, -0.06, 0.06, -0.07, 0.07, 0.06, -0.02, 0.1, -0.06, 0.08],
#                      "Telecommunication": [-0.06, -0.02, 0.13, -0.0, 0.1, -0.13, 0.02, 0.14, -0.1, 0.03, -0.12, 0.23,
#                                            0.04, 0.15, 0.04, -0.03, 0.08, -0.24, -0.0, -0.03, 0.2, 0.02, -0.14, 0.17],
#                      "Travel": [0.07, 0.25, 0.05, 0.13, 0.19, 0.03, 0.13, 0.14, 0.05, -0.14, -0.07, 0.2, -0.15, -0.02,
#                                 -0.08, -0.08, 0.07, -0.16, 0.06, 0.02, 0.18, -0.04, 0.19, -0.09],
#                      "Construction": [0.1, 0.03, -0.05, 0.0, 0.01, 0.03, -0.04, 0.02, 0.01, -0.41, 0.01, 0.19, 0.08,
#                                       0.15, -0.18, 0.1, 0.2, -0.25, -0.1, 0.04, -0.01, -0.34, 0.03, -0.28],
#                      "MassMediaBooks": [-0.16, 0.25, -0.12, 0.04, 1.11, -0.25, -0.33, 0.19, 0.55, -0.26, -0.5, -0.12,
#                                         0.13, 0.17, -0.14, -0.02, 1.01, -0.35, -0.3, -0.14, 0.67, -0.0, -0.52, -0.38],
#                      "Mobile home dealer": [0.07, 0.13, 0.07, -0.07, 0.21, -0.24, 0.15, 0.18, 0.2, -0.31, 0.03, 0.11,
#                                             0.0, 0.16, -0.13, 0.12, 0.04, -0.03, 0.1, 0.02, 0.13, -0.38, 0.06, -0.14],
#                      "SalvageRepairs": [0.11, 0.2, 0.06, -0.1, 0.14, -0.11, 0.01, 0.19, 0.33, -0.52, -0.14, 0.16, 0.12,
#                                         0.03, -0.2, -0.06, 0.15, -0.03, 0.03, -0.06, 0.12, -0.31, 0.01, -0.08],
#                      "Services": [3.1, 3.2, 3.5, 2.0, -0.5, -1, -1.5, -1.5, 0, 3, 4, 4.3],
#                      "Halls": [0.08, -0.01, -0.05, 0.13, 0.18, -0.1, 0.15, 0.07, 0.01, -0.16, -0.13, -0.01, -0.01, 0.02,
#                                -0.16, -0.02, 0.02, -0.04, 0.03, -0.05, -0.03, -0.31, -0.03, 0.26],
#                      "Sports": [0.03, 0.2, 0.1, -0.19, 0.27, -0.19, -0.15, 0.19, 0.19, -0.3, 0.05, 0.17, 0.09, 0.11,
#                                 0.21, 0.14, 0.08, -0.08, 0.1, 0.26, 0.22, -0.56, 0.02, -0.15],
#                      "Casinos": [0.15, 0.16, 0.14, -0.21, 0.53, -0.14, -0.27, -0.01, 0.53, -0.21, -0.34, 0.08, 0.02,
#                                  0.14, -0.08, -0.01, 0.29, -0.22, -0.08, -0.09, 0.35, -0.39, -0.08, -0.04],
#                      "Charity": [0.15, 0.12, -0.08, 0.04, 0.04, -0.08, -0.08, 0.0, 0.1, -0.16, -0.17, 0.15, 0.12, 0.01,
#                                  -0.12, 0.16, 0.01, -0.03, 0.17, -0.12, 0.16, 0.14, -0.0, 0.18],
#                      "AverageCardHolderSpent": [-0.1, 0.18, -0.14, -0.11, -0.07, -0.05, 0.13, -0.19, -0.01, 0.06, -0.02,
#                                                 -0.01, 0.07, 0.0, -0.2, -0.03, 0.11, -0.02, 0.04, -0.07, 0.04, -0.21,
#                                                 0.09, 0.27],
#                      "CardTransactionFrequency": [0.16, 0.2, 0.07, -0.16, 0.94, -0.36, -0.35, -0.08, 0.62, -0.21, -0.39,
#                                                   0.15, -0.01, -0.08, -0.12, -0.02, 1.0, -0.06, -0.38, -0.16, 0.73,
#                                                   -0.2, -0.26, -0.34],
#                      "EcommerceSpent": [0.08, 0.14, -0.16, 0.12, 0.23, -0.14, 0.2, -0.08, 0.01, -0.06, -0.15, 0.11,
#                                         -0.11, 0.19, -0.21, 0.13, -0.11, 0.03, 0.15, 0.06, -0.0, -0.06, -0.15, 0.09],
#                      "NonEcommerceSpent": [-0.1, 0.18, -0.09, -0.03, 0.61, -0.2, -0.22, 0.03, 0.55, -0.12, -0.23, 0.21,
#                                            -0.06, 0.02, 0.06, -0.05, 0.47, -0.35, -0.07, -0.05, 0.41, -0.18, -0.38,
#                                            0.0],
#                      "Top5HitCategories": ["Halls", "Casinos", "Charity", "Travel", "MassMediaBooks"],
#                      "OutCountryTransaction": [-0.01, 0.16, 0.18, -0.2, 0.19, -0.08, -0.15, 0.25, 0.45, -0.26, -0.0,
#                                                0.15, 0.04, -0.09, 0.05, -0.1, 0.06, -0.25, 0.02, 0.0, 0.52, -0.59,
#                                                -0.44, -0.06],
#                      "OutMSATransaction": [3.1, 3.2, 3.5, 2.0, -0.5, -1, -1.5, -1.5, 0, 3, 4, 4.3]}
#                     )

# print(json.loads(add_insights(json_v,json.dumps({"_id":640,"MSAName":"Austin","Population":964336,"EmployedPeople":677234,"RevenueLoss":[-0.12,0.05,0.2,-0.1,0.03,0.01,0.07,0.02,0.1,0.05,-0.14,0.02,0.07,-0.02,0.1,-0.08,-0.14,0.13,-0.0,0.16,0.05,0.18,-0.07,0.02],"GroceriesSuperMarket":[-0.07,0.11,-0.15,-0.07,-0.09,0.03,0.03,-0.05,0.04,-0.15,0.07,0.01,-0.11,-0.07,-0.06,-0.03,0.15,0.0,0.14,0.09,0.05,-0.05,-0.18,0.25],"Transportation":[0.09,0.25,-0.19,0.12,0.1,-0.1,-0.06,0.06,0.28,-0.0,-0.06,0.28,-0.05,0.02,-0.04,0.13,0.07,-0.07,-0.01,-0.01,0.24,-0.06,0.06,0.13],"Cabs":[-0.07,0.25,-0.19,0.1,-0.05,-0.07,-0.04,0.04,0.2,-0.28,-0.0,0.06,-0.06,-0.07,-0.19,0.19,-0.01,0.09,0.05,0.14,0.23,-0.25,0.09,0.29],"ShoesClothes":[-0.06,0.05,0.02,0.02,0.09,-0.1,0.11,0.1,0.15,-0.45,-0.02,0.08,0.13,0.06,-0.02,-0.1,-0.07,-0.26,0.15,0.1,0.32,-0.42,-0.09,0.04],"FoodDrink":[0.03,0.08,0.15,0.03,0.15,0.13,-0.13,0.04,-0.11,0.06,-0.14,-0.05,0.04,-0.1,0.07,-0.12,0.11,-0.1,0.06,0.15,-0.08,-0.04,0.08,-0.12,0.0],"Electronics":[-0.04,0.06,0.14,0.13,0.09,-0.01,0.21,-0.07,-0.12,-0.29,-0.08,0.05,-0.11,0.19,0.12,-0.03,0.03,0.02,-0.12,-0.12,0.03,-0.13,-0.1,-0.02],"Stationaries":[3.1,3.2,3.5,2.0,-0.5,-1,-1.5,-1.5,0,3,4,4.3],"Education":[-0.04,-0.05,-0.16,-0.16,0.02,-0.06,0.2,0.09,-0.08,-0.07,0.12,0.15,0.18,0.15,0.03,0.18,-0.01,-0.16,0.06,0.02,-0.06,-0.24,-0.03,-0.18],"Hardware":[-0.1,-0.02,-0.21,0.1,-0.01,-0.26,-0.1,0.1,0.08,-0.38,0.16,0.16,0.04,0.16,-0.21,0.15,-0.04,-0.01,-0.06,0.0,0.33,-0.23,0.2,-0.11],"Automobile":[0.08,0.03,0.03,-0.02,0.18,0.02,-0.01,0.08,0.17,-0.24,-0.05,0.37,-0.11,0.03,-0.04,0.15,0.1,-0.01,0.15,0.11,0.17,0.05,0.05,-0.29],"Medical":[-0.12,0.23,-0.12,-0.01,0.15,0.06,0.02,0.04,0.06,0.03,-0.12,0.09,0.09,-0.12,0.04,0.01,-0.12,0.13,-0.01,-0.02,0.12,-0.1,-0.06,0.29],"Religious":[0.15,-0.03,-0.13,0.0,0.05,-0.15,-0.11,0.19,0.21,-0.11,-0.21,0.15,-0.13,0.07,0.04,0.02,0.09,0.09,0.21,0.14,-0.0,0.03,0.1,0.31],"Petroleum":[-0.02,0.11,-0.03,-0.08,0.05,-0.03,0.21,-0.04,-0.07,0.06,-0.02,0.31,0.16,0.0,-0.02,-0.03,-0.04,-0.03,0.04,-0.06,-0.16,-0.04,-0.22,-0.07],"Telecommunication":[0.16,-0.01,-0.07,-0.06,0.12,-0.11,0.04,-0.12,-0.03,-0.01,0.07,-0.01,-0.04,-0.07,-0.06,0.04,0.05,0.04,0.07,-0.04,0.19,-0.22,-0.05,0.1],"Travel":[-0.07,0.2,0.09,-0.02,0.06,0.0,0.03,-0.13,-0.01,0.01,0.04,0.08,-0.21,0.07,-0.21,-0.05,0.01,-0.07,-0.06,0.09,-0.05,0.04,0.15,-0.0],"Construction":[-0.13,0.18,-0.17,0.09,-0.09,0.05,0.04,0.02,0.02,-0.35,-0.05,0.37,-0.19,-0.07,0.02,-0.13,0.13,-0.22,0.14,0.07,0.2,-0.07,-0.04,0.01],"MassMediaBooks":[-0.14,-0.02,-0.22,-0.0,0.97,-0.45,-0.17,-0.03,0.57,0.02,-0.3,-0.05,0.05,-0.04,-0.17,0.03,1.02,-0.08,-0.37,0.01,0.65,0.06,-0.44,-0.45],"Mobile home dealer":[0.07,0.23,-0.15,0.15,0.01,-0.11,-0.07,0.23,0.28,-0.27,0.05,0.24,0.1,0.11,0.01,0.17,0.09,-0.16,0.07,0.09,0.19,-0.43,-0.08,-0.05],"SalvageRepairs":[-0.05,0.03,0.09,-0.11,0.07,-0.19,0.06,0.1,0.02,-0.47,0.06,0.29,-0.15,0.02,-0.2,-0.0,0.08,-0.0,0.09,0.25,0.13,-0.39,-0.02,-0.15],"Services":[3.1,3.2,3.5,2.0,-0.5,-1,-1.5,-1.5,0,3,4,4.3],"Halls":[0.1,-0.01,-0.07,0.03,0.08,-0.09,-0.05,0.16,0.16,-0.29,-0.01,0.01,0.12,0.01,0.03,-0.1,0.11,-0.21,0.12,-0.11,0.15,-0.31,-0.19,0.3],"Sports":[0.01,0.08,-0.11,0.06,-0.03,-0.16,0.04,0.04,0.21,-0.29,-0.03,0.08,0.1,-0.08,-0.07,-0.09,0.2,-0.18,-0.14,0.13,0.44,-0.41,0.08,0.03],"Casinos":[0.13,0.16,-0.07,-0.05,0.43,-0.16,-0.2,0.29,0.3,-0.13,-0.33,0.04,0.05,0.11,-0.13,-0.03,0.55,-0.26,-0.06,-0.07,0.62,-0.25,-0.37,0.02],"Charity":[-0.06,0.07,-0.03,0.07,-0.08,-0.19,0.06,0.15,0.19,-0.04,-0.04,0.03,0.06,0.15,0.1,-0.04,-0.06,0.09,0.13,0.07,0.24,0.22,-0.08,0.01],"AverageCardHolderSpent":[-0.1,0.1,0.06,-0.08,0.13,-0.04,0.18,0.01,0.14,-0.05,-0.12,-0.02,0.06,0.03,0.05,0.12,-0.11,-0.01,-0.0,0.02,-0.01,-0.03,-0.12,0.27],"CardTransactionFrequency":[0.08,0.07,-0.19,-0.19,0.96,-0.29,-0.31,0.06,0.63,-0.14,-0.29,-0.08,-0.02,-0.0,0.02,0.1,1.01,-0.22,-0.24,-0.16,0.58,-0.1,-0.28,-0.36],"EcommerceSpent":[-0.05,-0.05,0.1,0.04,0.02,-0.25,0.21,0.04,-0.01,0.07,-0.21,0.17,0.14,0.08,-0.04,-0.0,-0.07,-0.06,0.09,-0.19,0.05,0.02,0.01,0.08],"NonEcommerceSpent":[0.01,0.11,0.06,-0.17,0.41,-0.33,-0.27,0.17,0.4,-0.35,-0.33,0.0,0.09,-0.05,-0.07,0.04,0.4,-0.2,-0.14,0.18,0.42,-0.33,-0.3,-0.19],"UnemploymentRate":[3.1,3.2,3.5,2.0,-0.5,-1,-1.5,-1.5,0,3,4,4.3],"Unemployment":[3.1,3.2,3.5,2.0,-0.5,-1,-1.5,-1.5,0,3,4,4.3],"Employment":[3.1,3.2,3.5,2.0,-0.5,-1,-1.5,-1.5,0,3,4,4.3],"LaborForce":[3.1,3.2,3.5,2.0,-0.5,-1,-1.5,-1.5,0,3,4,4.3],"EmploymentPopulationRatio":[3.1,3.2,3.5,2.0,-0.5,-1,-1.5,-1.5,0,3,4,4.3],"LaborForceParticipationRate":[3.1,3.2,3.5,2.0,-0.5,-1,-1.5,-1.5,0,3,4,4.3],"CivilianNoninstitutionalPopulation":[3.1,3.2,3.5,2.0,-0.5,-1,-1.5,-1.5,0,3,4,4.3],"Top5HitCategories":["Halls","Casinos","Charity","Travel","MassMediaBooks"],"OutCountryTransaction":[-0.0,0.16,-0.08,-0.1,0.22,-0.21,-0.12,0.35,0.31,-0.25,-0.04,0.06,-0.14,0.2,0.06,-0.05,0.21,-0.19,-0.14,0.02,0.24,-0.68,-0.43,0.04],"OutMSATransaction":[3.1,3.2,3.5,2.0,-0.5,-1,-1.5,-1.5,0,3,4,4.3],"Top5PerformingPincode":[73301,78652,78708,78712,78716],"Top5HitPincode":[73344,78653,78703,78709,78710]}
# ))))
