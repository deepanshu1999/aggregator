var express = require('express'),
	router 	= express.Router({mergeParams: true}),
	fetch 	= require('node-fetch'),
	sample	= require('../../data/sample');



// ROUTE FOR SHOWING MSA ANALYSIS
router.get('/', (req, res) => {

	var msa_code = req.query.msa_code;

	fetch(`http://localhost:8000/merchant/msa/data?msacode=${msa_code}`)
		.then((res) => res.json())
		.then((json) => {
			res.render('analysis/msa', {
				title: `MSA - ${json.Data.MSAName} Analysis`,
				data: json
			});
		})
		.catch((err) => {
			console.log(err.message);
			res.render('error/timedOut', {
				title: 'Request Timed Out'
			});
		});
});



module.exports = router;