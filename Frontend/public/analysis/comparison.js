// ***********************
// DRAWING THE CHARTS
// ***********************

var data = parsed_data.Data;
var length = Object.keys(data._id).length;
var list = Object.values(data._id);


// ***********************
// REVENUE CHART
// ***********************

var revenueObj = {
	title: 'Revenue Trend',
	columns: [],
	colors: {},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: ''
};


for(var i = 0; i < length; i++){
	var arr = data.RevenueLoss[list[i]];
	arr.unshift(list[i]);
	revenueObj.columns.push(arr);
}

revenueObj.id = '#lineChart_c1';
lineChart.draw(revenueObj);

revenueObj.id = '#barChart_c1';
barChart.draw(revenueObj);

revenueObj.id = '#areaSpline_c1';
areaChart.draw(revenueObj);



// ********************************
// AVERAGE CARDHOLDER SPENT CHART
// ********************************

var cardholderObj = {
	title: 'Average Cardholder Spent',
	columns: [],
	colors: {},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: ''
};


for(var i = 0; i < length; i++){
	var arr = data.AverageCardHolderSpent[list[i]];
	arr.unshift(list[i]);
	console.log(arr);
	cardholderObj.columns.push(arr);
}

cardholderObj.id = '#lineChart_c2';
lineChart.draw(cardholderObj);

cardholderObj.id = '#barChart_c2';
barChart.draw(cardholderObj);

cardholderObj.id = '#areaSpline_c2';
areaChart.draw(cardholderObj);



// ************************************
// AVERAGE TRANSACTION FREQUENCY CHART
// ************************************	

var transactionObj = {
	title: 'Average Transaction Frequency',
	columns: [],
	colors: {},
	x_label: 'Months',
	y_label: 'Relative Values',
	point: [],
	id: ''
};


for(var i = 0; i < length; i++){
	var arr = data.CardTransactionFrequency[list[i]];
	arr.unshift(list[i]);
	transactionObj.columns.push(arr);
}


transactionObj.id = '#lineChart_c3';
lineChart.draw(transactionObj);

transactionObj.id = '#barChart_c3';
barChart.draw(transactionObj);

transactionObj.id = '#areaSpline_c3';
areaChart.draw(transactionObj);


// Toggle functions

function showDiv1(l,b,a){
	document.getElementById(l).style.display="block";
	document.getElementById(a).style.display="none";
	document.getElementById(b).style.display="none";
}	

function showDiv2(l,b,a){
	document.getElementById(l).style.display="none";
	document.getElementById(a).style.display="block";
	document.getElementById(b).style.display="none";
}	

function showDiv3(l,b,a){
	document.getElementById(l).style.display="none";
	document.getElementById(a).style.display="none";
	document.getElementById(b).style.display="block";
}	