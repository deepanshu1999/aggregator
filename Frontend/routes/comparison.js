var express = require('express'),
	router 	= express.Router({mergeParams: true}),
	fetch 	= require('node-fetch'),
	sample	= require('../data/sample');



// ROUTE FOR SHOWING COMPARISON FORM
router.get('/', (req, res) => {

	var url = 'http://localhost:8000/merchant/compare?level=';

	var granularity = req.query.level;
	granularity = granularity[0].toUpperCase() + granularity.slice(1) + 's';

	for(var key in req.query){
		if(req.query[key]){
			if(key == 'level'){
				url = url + req.query[key];
				if(req.query[key] == 'pincode'){
					req.query[key] = 'pin';
				}
			}
			else{
				url = url + `&${req.query['level']}code=${req.query[key]}`;
			}
		}
	}

	fetch(url)
		.then(res => res.json()).
		then(json => {
			var comparators = (Object.keys(json.Data._id)).join(', ');
			res.render('comparison/show', {
				title: `Comparison of ${granularity} ${comparators}`,
				data: json,
				comparators: comparators,
				granularity: granularity
			});
		})
		.catch(err => {
			console.log(err.message);
			res.render('error/timedOut', {
				title: 'Request Timed Out'
			});
		});
});



module.exports = router;