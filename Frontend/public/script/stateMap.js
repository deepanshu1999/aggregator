var list = {
	"Data": {
		"840": [{
				"subDivisionCode": "US-AL",
				"State": "Alabama",
				"Population": "49,03,185",
				"RevenueLoss": -2.4784441772506827
			},
			{
				"subDivisionCode": "US-AK",
				"State": "Alaska",
				"Population": "7,31,545",
				"RevenueLoss": -2.8070555866576097
			},
			{
				"subDivisionCode": "US-AZ",
				"State": "Arizona",
				"Population": "72,78,717",
				"RevenueLoss": -4.287300587627546
			},
			{
				"subDivisionCode": "US-AR",
				"State": "Arkansas",
				"Population": "30,17,804",
				"RevenueLoss": -0.5366154203705706
			},
			{
				"subDivisionCode": "US-CA",
				"State": "California",
				"Population": "3,95,12,223",
				"RevenueLoss": -1.6445564941865118
			},
			{
				"subDivisionCode": "US-CO",
				"State": "Colorado",
				"Population": "57,58,736",
				"RevenueLoss": -1.0456470597880898
			},
			{
				"subDivisionCode": "US-CT",
				"State": "Connecticut",
				"Population": "35,65,287",
				"RevenueLoss": 2.4738426250203007
			},
			{
				"subDivisionCode": "US-DE",
				"State": "Delaware",
				"Population": "9,73,764",
				"RevenueLoss": 3.8545576770782084
			},
			{
				"subDivisionCode": "US-FL",
				"State": "Florida",
				"Population": "2,14,77,737",
				"RevenueLoss": -4.375316321047194
			},
			{
				"subDivisionCode": "US-GA",
				"State": "Georgia",
				"Population": "1,06,17,423",
				"RevenueLoss": -0.36565715589425984
			},
			{
				"subDivisionCode": "US-HI",
				"State": "Hawaii",
				"Population": "14,15,872",
				"RevenueLoss": 3.2066889112444805
			},
			{
				"subDivisionCode": "US-ID",
				"State": "Idaho",
				"Population": "17,87,065",
				"RevenueLoss": 4.963220015113146
			},
			{
				"subDivisionCode": "US-IL",
				"State": "Illinois",
				"Population": "1,26,71,821",
				"RevenueLoss": 4.188288559907914
			},
			{
				"subDivisionCode": "US-IN",
				"State": "Indiana",
				"Population": "67,32,219",
				"RevenueLoss": -2.6217587074762463
			},
			{
				"subDivisionCode": "US-IA",
				"State": "Iowa",
				"Population": "31,55,070",
				"RevenueLoss": 2.109629561221257
			},
			{
				"subDivisionCode": "US-KS",
				"State": "Kansas",
				"Population": "29,13,314",
				"RevenueLoss": 1.4467074535712996
			},
			{
				"subDivisionCode": "US-KY",
				"State": "Kentucky",
				"Population": "44,67,673",
				"RevenueLoss": 4.20850422367527
			},
			{
				"subDivisionCode": "US-LA",
				"State": "Louisiana",
				"Population": "46,48,794",
				"RevenueLoss": -3.6457493315415146
			},
			{
				"subDivisionCode": "US-ME",
				"State": "Maine",
				"Population": "13,44,212",
				"RevenueLoss": -4.344408520557071
			},
			{
				"subDivisionCode": "US-MD",
				"State": "Maryland",
				"Population": "60,45,680",
				"RevenueLoss": 0.7044943366801331
			},
			{
				"subDivisionCode": "US-MA",
				"State": "Massachusetts",
				"Population": "68,92,503",
				"RevenueLoss": -3.405240722178884
			},
			{
				"subDivisionCode": "US-MI",
				"State": "Michigan",
				"Population": "99,86,857",
				"RevenueLoss": -2.4610465110292914
			},
			{
				"subDivisionCode": "US-MN",
				"State": "Minnesota",
				"Population": "56,39,632",
				"RevenueLoss": -0.2991886060139759
			},
			{
				"subDivisionCode": "US-MS",
				"State": "Mississippi",
				"Population": "29,76,149",
				"RevenueLoss": -4.3758714489974935
			},
			{
				"subDivisionCode": "US-MO",
				"State": "Missouri",
				"Population": "61,37,428",
				"RevenueLoss": 0.1454369608640862
			},
			{
				"subDivisionCode": "US-MT",
				"State": "Montana",
				"Population": "10,68,778",
				"RevenueLoss": -1.4199365417376142
			},
			{
				"subDivisionCode": "US-NE",
				"State": "Nebraska",
				"Population": "19,34,408",
				"RevenueLoss": 4.513517212949019
			},
			{
				"subDivisionCode": "US-NV",
				"State": "Nevada",
				"Population": "30,80,156",
				"RevenueLoss": -2.3098738701026624
			},
			{
				"subDivisionCode": "US-NH",
				"State": "New Hampshire",
				"Population": "13,59,711",
				"RevenueLoss": 2.174870363390907
			},
			{
				"subDivisionCode": "US-NJ",
				"State": "New Jersey",
				"Population": "88,82,190",
				"RevenueLoss": -0.2583789368318694
			},
			{
				"subDivisionCode": "US-NM",
				"State": "New Mexico",
				"Population": "20,96,829",
				"RevenueLoss": -4.2371164852034005
			},
			{
				"subDivisionCode": "US-NY",
				"State": "New York",
				"Population": "1,94,53,561",
				"RevenueLoss": -0.3086295325797872
			},
			{
				"subDivisionCode": "US-NC",
				"State": "North Carolina",
				"Population": "1,04,88,084",
				"RevenueLoss": 2.7889214173583454
			},
			{
				"subDivisionCode": "US-ND",
				"State": "North Dakota",
				"Population": "7,62,062",
				"RevenueLoss": -3.6121934479283624
			},
			{
				"subDivisionCode": "US-OH",
				"State": "Ohio",
				"Population": "1,16,89,100",
				"RevenueLoss": 4.560088365153337
			},
			{
				"subDivisionCode": "US-OK",
				"State": "Oklahoma",
				"Population": "39,56,971",
				"RevenueLoss": -2.971727844531821
			},
			{
				"subDivisionCode": "US-OR",
				"State": "Oregon",
				"Population": "42,17,737",
				"RevenueLoss": -4.226393728631828
			},
			{
				"subDivisionCode": "US-PA",
				"State": "Pennsylvania",
				"Population": "1,28,01,989",
				"RevenueLoss": -4.269848996983851
			},
			{
				"subDivisionCode": "US-RI",
				"State": "Rhode Island",
				"Population": "10,59,361",
				"RevenueLoss": -4.135193929232129
			},
			{
				"subDivisionCode": "US-SC",
				"State": "South Carolina",
				"Population": "51,48,714",
				"RevenueLoss": 4.957817141226787
			},
			{
				"subDivisionCode": "US-SD",
				"State": "South Dakota",
				"Population": "8,84,659",
				"RevenueLoss": 1.7867911551536189
			},
			{
				"subDivisionCode": "US-TN",
				"State": "Tennessee",
				"Population": "68,29,174",
				"RevenueLoss": -2.092912793378634
			},
			{
				"subDivisionCode": "US-TX",
				"State": "Texas",
				"Population": "2,89,95,881",
				"RevenueLoss": 2.1986629282084946
			},
			{
				"subDivisionCode": "US-UT",
				"State": "Utah",
				"Population": "32,05,958",
				"RevenueLoss": -0.9796623551147432
			},
			{
				"subDivisionCode": "US-VT",
				"State": "Vermont",
				"Population": "6,23,989",
				"RevenueLoss": 0.5298358139333175
			},
			{
				"subDivisionCode": "US-VA",
				"State": "Virginia",
				"Population": "85,35,519",
				"RevenueLoss": -1.3765120171185785
			},
			{
				"subDivisionCode": "US-WA",
				"State": "Washington",
				"Population": "76,14,893",
				"RevenueLoss": 0.9887169926597394
			},
			{
				"subDivisionCode": "US-WV",
				"State": "West Virginia",
				"Population": "17,92,147",
				"RevenueLoss": 1.4220576875034494
			},
			{
				"subDivisionCode": "US-WI",
				"State": "Wisconsin",
				"Population": "58,22,434",
				"RevenueLoss": -1.0668060847205574
			},
			{
				"subDivisionCode": "US-WY",
				"State": "Wyoming",
				"Population": "5,78,759",
				"RevenueLoss": 2.926812755258271
			},
			{
				"subDivisionCode": "US-DC",
				"State": "District of Columbia",
				"Population": "7,05,749",
				"RevenueLoss": -2.689602898119273
			}
		]
	}
};


$(document).ready(function () {

});

var state_data = {
	MD: {fill: '#A4AA88'},
	VA: {fill: '#A4AA88', fullname:'Varginia', src: 'http://raphaeljs.com/'}
};

$('#map').usmap({
	stateStyles: {
		fill: 'white'
	},
	stateHoverStyles: {
		fill: 'teal'
	},
	stateHoverAnimation: 0,
	showLabels: false
	// stateSpecificStyles: state_data
	//stateSpecificHoverStyles: {MD: {fill: 'red'}}
});


// max and min values
// 4.963220015113146
// -4.3758714489974935

$('#map').on('usmapmouseover', function (event, data) {

	var state_code = 'US-' + data.name;
	let state = list.Data['840'].find(o => o.subDivisionCode === state_code);

	$("countryToState").html("State Name")
	$("#nameState").html(state.State);
	$("#Revenue").html(state.RevenueLoss.toFixed(4));
	$("#Population").html(state.Population);

});
$('#map').on('usmapmouseout', function (event, data) {
	
	var state_code = 'US-' + data.name;
	let state = list.Data['840'].find(o => o.subDivisionCode === state_code);
	$("countryToState").html("Country Name")
	$("#nameState").html("USA");
	$("#Revenue").html("2.84");
	$("#Population").html("17,63,885");

});


$('#map').on('usmapclick', function (event, data) {
	var state_code = 'US-' + data.name;
	window.location = `http://localhost:2000/analysis/state?state_code=${state_code}`
});