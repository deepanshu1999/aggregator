# views.pyfrom rest_framework import viewsets

from django.http import JsonResponse
import requests
#from .API import Utils
from .API.Database import *


def home(request):
	return JsonResponse({"1":"Author: Deepanshu Aggarwal"},status=0)


def getcountrydata(request):
	try:
		det=get_country_data(request.GET["countrycode"])
		return JsonResponse({"Data":det},status=200)
	except Exception as inst:
		return JsonResponse({"Error":inst.args[1]},status=inst.args[0])



def getstatelist(request):
	try:
		res=get_state_list()
		return JsonResponse({"Data":res},status=200)
	except Exception as inst:
		return JsonResponse({"Error":inst.args[1]},status=inst.args[0])
def getstatedata(request):
	try:
		det=get_state_data(request.GET["statecode"])
		return JsonResponse({"Data":det},status=200)
	except Exception as inst:
		return JsonResponse({"Error":inst.args[1]},status=inst.args[0])


def getpincodelist(request):
	try:
		lst=get_pincode_list(request.GET["statecode"])
		return JsonResponse({"Data":lst},status=200)
	except Exception as inst:
		return JsonResponse({"Error":inst.args[1]},status=inst.args[0])

def getpincodedata(request):
	try:
		det=get_pincode_data(request.GET["pincode"])
		return JsonResponse({"Data":det},status=200)
	except Exception as inst:
		return JsonResponse({"Error":inst.args[1]},status=inst.args[0])

def getmsalist(request):
	try:
		res=get_msa_list()
		return JsonResponse({"Data":res},status=200)
	except Exception as inst:
		return JsonResponse({"Error":inst.args[1]},status=inst.args[0])

def getmsadata(request):
	try:
		det=get_msa_data(request.GET["msacode"])
		return JsonResponse({"Data":det},status=200)
	except Exception as inst:
		return JsonResponse({"Error":inst.args[1]},status=inst.args[0])

def compare(request):
	try:
		granularity=request.GET["level"]
	except:
		return JsonResponse({"Error":"Bad Parameters"},status=412)
	if(granularity=="country"):
		return JsonResponse({"Error":"Currently we do not support country comparison"},status=400)
	if(granularity=="state"):
		try:
			list_states=request.GET.getlist("statecode")
			det=get_comparison_states(list_states)
			return JsonResponse({"Data":det})
		except:
			return JsonResponse({"Error":"Error Fetching States"})
	if(granularity=="msa"):
		try:
			list_states=request.GET.getlist("msacode")
			det=get_comparison_msa(list_states)
			return JsonResponse({"Data":det})
		except:
			return JsonResponse({"Error":"Error Fetching MSA"})
	if(granularity=="pincode"):
		#try:
			list_states=request.GET.getlist("pincode")
			det=get_comparison_pincode(list_states)
			return JsonResponse({"Data":det})
		#except:
			return JsonResponse({"Error":"Error Fetching Pincode"})
	return JsonResponse({"Error":"Invalid Level"},status=400)

def aggregate(request):
	pass