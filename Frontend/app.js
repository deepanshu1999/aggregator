var express 		= require('express'),
	app 			= express(),
	path 			= require('path'),
	fetch 			= require('node-fetch'),
	flash 			= require('connect-flash'),
	request 		= require('request'),
	bodyParser 		= require('body-parser'),
	cookieParser 	= require('cookie-parser'),
	methodOverride 	= require('method-override');


//INITIALIZING CONSTANTS
const PORT = process.env.PORT || 2000;


//BASIC CONFIGURATIONS
app.use(flash());
app.use(cookieParser());
app.set("view engine", "ejs");
app.use(express.static(path.join(__dirname, "public")));
app.use(methodOverride("_method"));
app.use(bodyParser.urlencoded({
	extended: true
}));
// app.use(methodOverride('X-HTTP-Method-Override'));// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT


//MADE AVAILABLE FOR EACH TEMPLATE
app.use(function (req, res, next) {
	// res.locals.error = req.flash("error");
	// res.locals.success = req.flash("success");
	next();
});


//REQUIRING ROUTES
app.use("/", require("./routes/index"));


app.listen(PORT, () => {
	console.log('Server has started!!!');
});