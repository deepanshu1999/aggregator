var express = require('express'),
	router = express.Router({
		mergeParams: true
	}),
	fetch = require('node-fetch'),
	sample = require('../data/sample');



router.get('/', (req, res) => {
	res.render('worldmap', {
		title: 'world map'
	});
});



// REQUIRING ROUTE FOR HOME PAGE
router.use('/home', require('./home'));



// REQUIRING ROUTES FOR ANALYSIS OF STATE, MSA AND PINCODE
router.use('/analysis', require('./analysis/index'));


// REQUIRING ROUTE FOR COMPARISON OF STATES / MSA / PINCODES
router.use('/compare', require('./comparison'));


// ROUTE FOR INCORRECT URL
router.get('*', (req, res) => {
	res.render('error/error404', {
		title: 'Error 404'
	});
});



module.exports = router;