var express = require('express'),
	router 	= express.Router({mergeParams: true}),
	fetch 	= require('node-fetch'),
	sample	= require('../../data/sample');



// REQUIRING ROUTE FOR ANALYSIS OF STATE
router.use('/state', require('./state'));


// REQUIRING ROUTE FOR ANALYSIS OF MSA
router.use('/msa', require('./msa'));


// REQUIRING ROUTE FOR ANALYSIS OF PINCODE
router.use('/state/pincode', require('./pincode'))



module.exports = router;