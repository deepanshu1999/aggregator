var areaChart = {};

areaChart.draw = function(data) {
	var chart = bb.generate({
		title: {
			text: data.title
		},
		data: {
			json: data.json,
			names: data.names,
			type: 'area-spline'
		},
		point: {
			pattern: data.point
		},
		legend: {
			usePoint: true
		},
		area: {
			linearGradient: true
		},
		zoom: {
			enabled: {
				type: 'drag'
			}
		},
		axis: {
			x: {
				label: {
					text: data.x_label,
					position: 'outer-center'
				}
			},
			y: {
				label: {
					text: data.y_label,
					position: 'outer-middle'
				}
			}
		},
		bindto: data.id
	});
}