"""Author: Deepanshu Aggarwal
	Date: 22 JUN 2020
	class for the JSON Response from Merchant Measurement API

	{'responseHeader': 
		{'numRecordsReturned': 1, 
		'requestMessageId': '6da60e1b8b024532a2e0eacb1af58581',
	 	'messageDateTime': '2020-06-22 04:38:47.781', 
	 	'responseMessageId': 'MRCHBMARK_642f3ad3-af53-4d40-a1e5-7f9b4ccb72a3'}, 
	 'responseStatus': 
	 	{'statusDescription': 'Merchant Benchmark - Success', 
	 	'status': 'CDI000'}, 
	 'requestData': 
	 	{'cardPresentIndicator': 'CARDPRESENT',
	 	 'merchantCountry': '840', 
	 	 'groupList': ['STANDARD'], 
	 	 'merchantCategoryCodeList': ['5812'], 
	 	 'merchantCategoryGroupsCodeList': [''], 
	 	 'monthList': ['201706'], 'msaList': ['0120'], 
	 	 'postalCodeList': [''], 'naicsCodeList': [''], 
	 	 'countrySubdivisionList': [''], 
	 	 'accountFundingSourceList': ['ALl'], 
	 	 'eciIndicatorList': ['All'], 
	 	 'platformIDList': ['All'], 
	 	 'posEntryModeList': ['All']}, 
	 'response': 
	 	{'responseData': [
	 	{'salesTranCntGrowthMoM': '', 
	 	'salesVolumeGrowthMoM': '', 
	 	'salesTranCntGrowthYoY': '', 
	 	'salesVolumeGrowthYoY': '', 
	 	'groupName': 'standard',
	 	'fraudChbktoSalesGrowthYoY': '',
	 	'nonfraudChbktoSalesGrowthYoY': '', 
	 	'fraudChbktoSalesRatio': '', 
	 	'nonfraudChbktoSalesRatio': '',
	 	'ticketGrowthMoM': '', 
	 	'ticketGrowthYoY': ''}]}}
"""
import json
class Response:


	class ResponseHeader:
		def __init__(self,x):
			self.numRecordsReturned=x['numRecordsReturned']
			self.requestMessageId=x['requestMessageId']
			self.messageDateTime=x['messageDateTime']
			self.responseMessageId=x['responseMessageId']


	class ResponseStatus:
		def __init__(self,x):
			self.statusDescription=x['statusDescription']
			self.status=x['CDI000']


	class RequestData:
		def __init__(self,x):
			self.cardPresentIndicator=x['cardPresentIndicator']
			self.merchantCountry=x['merchantCountry']
			self.groupList=x['groupList']
			self.merchantCategoryCodeList=x['merchantCategoryCodeList']
			self.merchantCategoryGroupsCodeList=x['merchantCategoryGroupsCodeList']
			self.monthList=x['monthList']
			self.msaList=x['msaList']
			self.postalCodeList=x['postalCodeList']
			self.naicsCodeList=x['naicsCodeList']
			self.countrySubdivisionList=x['countrySubdivisionList']
			self.accountFundingSourceList=x['accountFundingSourceList']
			self.eciIndicatorList=x['eciIndicatorList']
			self.platformIDList=x['platformIDList']
			self.posEntryModeList=x['posEntryModeList']
	class Obj:
		def __init__(self,x):
			self.salesVolumeGrowthMoM=x['salesVolumeGrowthMoM']
			self.salesTranCntGrowthMoM=x['salesTranCntGrowthMoM']
			self.salesVolumeGrowthYoY=x['salesVolumeGrowthYoY']
			self.salesTranCntGrowthYoY=x['salesTranCntGrowthYoY']
			self.groupName=x['groupName']
			self.fraudChbktoSalesGrowthYoY=x['fraudChbktoSalesGrowthYoY']
			self.nonfraudChbktoSalesGrowthYoY=x['nonfraudChbktoSalesGrowthYoY']
			self.fraudChbktoSalesRatio=x['fraudChbktoSalesRatio']
			self.nonfraudChbktoSalesRatio=x['nonfraudChbktoSalesRatio']
			self.ticketGrowthYoY=x['ticketGrowthYoY']
			self.ticketGrowthMoM=x['ticketGrowthMoM']
	def __init__(self,X):
		self.responseHeader=self.ResponseHeader(X["responseHeader"])
		self.responseStatus=self.ResponseStatus(X["responseStatus"])
		self.requestData=self.RequestData(X['requestData'])
		self.response=[self.Obj(x) for x in X['response']['responseData']]
		self.jsonstore=X
	def tojson():
		return self.jsonstore

