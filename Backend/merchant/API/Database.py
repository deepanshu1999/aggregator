from merchant.API.Codes import Codes
import os
import json
import pymongo
import pickle
from merchant.API.Wrapper import add_insights

def get_pincode_list(state_code):
	try:
		with open(os.getcwd()+'/merchant/values/pincode.json',"r") as f:
			ans=json.loads(f.read())
	except:
		raise Exception(511,"Could not load list of pincodes")
	try:
		return ans[state_code]
	except:
		raise Exception(412,"Wrong State Code")
	

def get_state_code(pincode):
	js_data = {}
	with open(os.getcwd()+"/merchant/values/pincode.json", 'r') as fp:
		js_data = json.loads(fp.read())
	for state_code in js_data.keys():
		for pincode_data in js_data[state_code]:
			if int(pincode_data["pincode"]) == int(pincode):
				return state_code
	raise Exception(414,"pincode not found")



def get_state_list():
	try:
		with open(os.getcwd()+'/merchant/values/state.p',"rb") as f:
			ans=pickle.loads(f.read())
		return ans
	except:
		raise Exception(511,"Could not load list of states.")


def get_country_data(country_code):
	try:
		myclient = pymongo.MongoClient("mongodb://localhost:27017/")
	except:
		raise Exception(513,"Could not connect to Database")

	mydb=myclient["Aggregator"]
	mycl=mydb["Country"]
	query={"_id":int(country_code)}
	mydoc=mycl.find(query)
	mydoc=list(mydoc)
	if(len(mydoc)==0):
		raise Exception(414,"No record Found")
	doc_with_insights = add_insights(mydoc[0])
	return doc_with_insights

def get_state_data(state_code):
	try:
		myclient = pymongo.MongoClient("mongodb://localhost:27017/")
	except:
		raise Exception(513,"Could not connect to Database")
	#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
	mydb=myclient["Aggregator"]
	mycl=mydb["State"]
	query={"_id":state_code}
	mydoc=mycl.find(query)
	mydoc=list(mydoc)
	if(len(mydoc)==0):
		raise Exception(414,"No record Found")

	country_data = get_country_data(country_code = 840)
	doc_with_insights = add_insights(mydoc[0],country_data)
	return doc_with_insights

def get_msa_data(msa_code):
	try:
		myclient = pymongo.MongoClient("mongodb://localhost:27017/")
	except:
		raise Exception(513,"Could not connect to Database")

	mydb=myclient["Aggregator"]
	mycl=mydb["MSA"]
	query={"_id":int(msa_code)}
	mydoc=mycl.find(query)
	mydoc=list(mydoc)
	if(len(mydoc)==0):
		raise Exception(414,"No record Found")

	country_data = get_country_data(country_code = 840)
	doc_with_insights = add_insights(mydoc[0], country_data)
	return doc_with_insights

def get_msa_list():
	try:
		with open(os.getcwd()+'/merchant/values/msa.p',"rb") as f:
			ans=pickle.loads(f.read())
	except:
		raise Exception(511,"Could not load list of MSAs")

def get_pincode_data(pincode):
	try:
		myclient = pymongo.MongoClient("mongodb://localhost:27017/")
	except:
		raise Exception(513,"Could not connect to Database")
	mydb=myclient["Aggregator"]
	mycl=mydb["Pincode"]
	query={"_id":int(pincode)}
	mydoc=mycl.find(query)
	mydoc=list(mydoc)
	if(len(mydoc)==0):
		raise Exception(414,"No record Found")

	state_code = get_state_code(pincode = pincode)
	state_data = get_state_data(state_code = state_code)
	doc_with_insights =add_insights(mydoc[0],state_data)

	return doc_with_insights

def get_comparison_states(list_states):
	try:
		myclient = pymongo.MongoClient("mongodb://localhost:27017/")
	except:
		raise Exception(513,"Could not connect to Database")
	mydb=myclient["Aggregator"]
	mycl=mydb["State"]
	x=mycl.find({"_id":{"$in": list_states}})
	doc=list(x)
	if(len(doc)==0):
		raise Exception(414,"No record Found")
	
	combined_response={}
	for key in doc[0].keys():
		lrs={}
		for i in range(len(doc)):
			lrs[doc[i]["_id"]]=doc[i][key]
		combined_response[key]=lrs
	return combined_response

def get_comparison_msa(list_msa):
	try:
		myclient = pymongo.MongoClient("mongodb://localhost:27017/")
	except:
		raise Exception(513,"Could not connect to Database")
	mydb=myclient["Aggregator"]
	mycl=mydb["MSA"]
	x=mycl.find({"_id":{"$in": [int(y) for y in list_msa]}})
	doc=list(x)
	if(len(doc)==0):
		raise Exception(414,"No record Found")
	
	combined_response={}
	for key in doc[0].keys():
		lrs={}
		for i in range(len(doc)):
			lrs[doc[i]["_id"]]=doc[i][key]
		combined_response[key]=lrs
	return combined_response

def get_comparison_pincode(list_pincodes):
	try:
		myclient = pymongo.MongoClient("mongodb://localhost:27017/")
	except:
		raise Exception(513,"Could not connect to Database")
	mydb=myclient["Aggregator"]
	mycl=mydb["Pincode"]
	x=mycl.find({"_id":{"$in": [int(y) for y in list_pincodes]}})
	print({"_id":{"$in": list_pincodes}})
	doc=list(x)
	if(len(doc)==0):
		raise Exception(414,"No record Found")
	
	combined_response={}
	for key in doc[0].keys():
		lrs={}
		for i in range(len(doc)):
			lrs[doc[i]["_id"]]=doc[i][key]
		combined_response[key]=lrs
	return combined_response	
