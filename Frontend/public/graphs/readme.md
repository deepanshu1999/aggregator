//Sample function call for graphs

var obj = {
	title: 'Your title',
	json: {
		//your data field
		data1: [],
		data2: []	// and so on
	},
	names: {
		// names for the json data fields to be displayed in legend
		data1: '',
		data2: ''
	},
	color: {
		// colors for each data series
		data1: '',
		data2: ''
	},
	x_label: '',
	y_label: '',
	point = [
		//shape of pointers
		'circle',
		'rectangle'
	],
	id: '#chart_id'
}

lineChart.draw(obj);



Sample pointers:-
	"circle"
	"rectangle"
	"<polygon points='2.5 0 0 5 5 5'></polygon>"
	"<polygon points='2.5 0 0 2.5 2.5 5 5 2.5 2.5 0'></polygon>"
	"<g><circle cx='10' cy='10' r='10'></circle><rect x='5' y='5' width='10' height='10' 