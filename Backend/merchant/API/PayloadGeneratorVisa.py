import RequestVisa
import datetime


def getdefaultpayload():
	date = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]+'7Z'
	payload_dict={
		"requestHeader":
		{
			"messageDateTime": str(date),
			"requestMessageId": "6da60e1b8b024532a2e0eacb1af58581"
		},
		"requestData":
		{
			"naicsCodeList": [""],
			"merchantCategoryCodeList": [""],
			"merchantCategoryGroupsCodeList": [""],
			"postalCodeList": [""],
			"msaList": [""],
			"countrySubdivisionList": [""],
			"merchantCountry": "840",
			"monthList": [],
			"accountFundingSourceList": ["All"],
			"eciIndicatorList": ["All"],
			"platformIDList": ["All"],
			"posEntryModeList": ["All"],
			"cardPresentIndicator": "ALL",
			"groupList": ["STANDARD","CARDHOLDER"]
		}
	}
	return payload_dict


def loaders_state(state_code,month_list):
	payload=getdefaultpayload()
	payload["requestData"]["countrySubdivisionList"]=[state_code]
	#payload["requestData"]["monthList"]=[month]
	# TODO: Fill or Load these
	category_list=[]
	dic={}
	for x in category_list:
		lp=[]
		lt=[]
		la=[]
		ecom=[]
		nonecom=[]
		for y in month_list:
			if(x[0]=='RevenueLoss'):
				payload["requestData"]["monthList"]=[y]
				payload["requestData"]["merchantCategoryGroupsCodeList"]=x[1]
				response=RequestVisa.get_data(payload)
				lp.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
				if(x[0]=='RevenueLoss'):
					lt.append(response["response"]["responseData"]["avgCardTranFreq"])
					la.append(response["response"]["responseData"]["avgCardholderSpend"])
					payload["cardPresentIndicator"]="PRESENT"
					response=RequestVisa.get_data(payload)
					ecom.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
					payload["cardPresentIndicator"]="NOTPRESENT"
					response=RequestVisa.get_data(payload)
					nonecom.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
		dic[x[0]]=lp
		if(x[0]=='RevenueLoss'):
			dic["AverageCardholderSpent"]=la
			dic["AverageTransactionFrequency"]=lt
			dic["EcommerceSpent"]=ecom
			dic["NonEcommerceSpent"]=nonecom
	return dic

def loaders_msa(msa_code,month_list):
	payload=getdefaultpayload()
	payload["requestData"]["msaList"]=[state_code]
	#payload["requestData"]["monthList"]=[month]
	# TODO: Fill or Load these
	category_list=[]
	dic={}
	for x in category_list:
		lp=[]
		lt=[]
		la=[]
		ecom=[]
		nonecom=[]
		for y in month_list:
			if(x[0]=='RevenueLoss'):
				payload["requestData"]["monthList"]=[y]
				payload["requestData"]["merchantCategoryGroupsCodeList"]=x[1]
				response=RequestVisa.get_data(payload)
				lp.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
				if(x[0]=='RevenueLoss'):
					lt.append(response["response"]["responseData"]["avgCardTranFreq"])
					la.append(response["response"]["responseData"]["avgCardholderSpend"])
					payload["cardPresentIndicator"]="PRESENT"
					response=RequestVisa.get_data(payload)
					ecom.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
					payload["cardPresentIndicator"]="NOTPRESENT"
					response=RequestVisa.get_data(payload)
					nonecom.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
		dic[x[0]]=lp
		if(x[0]=='RevenueLoss'):
			dic["AverageCardholderSpent"]=la
			dic["AverageTransactionFrequency"]=lt
			dic["EcommerceSpent"]=ecom
			dic["NonEcommerceSpent"]=nonecom
	return dic

def loaders_pincode(pin_code,month_list):
	payload=getdefaultpayload()
	payload["requestData"]["postalCodeList"]=[state_code]
	#payload["requestData"]["monthList"]=[month]
	# TODO: Fill or Load these
	category_list=[]
	dic={}
	for x in category_list:
		lp=[]
		lt=[]
		la=[]
		ecom=[]
		nonecom=[]
		for y in month_list:
			if(x[0]=='RevenueLoss'):
				payload["requestData"]["monthList"]=[y]
				payload["requestData"]["merchantCategoryGroupsCodeList"]=x[1]
				response=RequestVisa.get_data(payload)
				lp.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
				if(x[0]=='RevenueLoss'):
					lt.append(response["response"]["responseData"]["avgCardTranFreq"])
					la.append(response["response"]["responseData"]["avgCardholderSpend"])
					payload["cardPresentIndicator"]="PRESENT"
					response=RequestVisa.get_data(payload)
					ecom.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
					payload["cardPresentIndicator"]="NOTPRESENT"
					response=RequestVisa.get_data(payload)
					nonecom.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
		dic[x[0]]=lp
		if(x[0]=='RevenueLoss'):
			dic["AverageCardholderSpent"]=la
			dic["AverageTransactionFrequency"]=lt
			dic["EcommerceSpent"]=ecom
			dic["NonEcommerceSpent"]=nonecom
	return dic

def loaders_country(month_list):
	payload=getdefaultpayload()
	#payload["requestData"]["monthList"]=[month]
	# TODO: Fill or Load these
	category_list=[]
	dic={}
	for x in category_list:
		lp=[]
		lt=[]
		la=[]
		ecom=[]
		nonecom=[]
		for y in month_list:
			if(x[0]=='RevenueLoss'):
				payload["requestData"]["monthList"]=[y]
				payload["requestData"]["merchantCategoryGroupsCodeList"]=x[1]
				response=RequestVisa.get_data(payload)
				lp.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
				if(x[0]=='RevenueLoss'):
					lt.append(response["response"]["responseData"]["avgCardTranFreq"])
					la.append(response["response"]["responseData"]["avgCardholderSpend"])
					payload["cardPresentIndicator"]="PRESENT"
					response=RequestVisa.get_data(payload)
					ecom.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
					payload["cardPresentIndicator"]="NOTPRESENT"
					response=RequestVisa.get_data(payload)
					nonecom.append(response["response"]["responseData"]["salesVolumeGrowthMoM"])
		dic[x[0]]=lp
		if(x[0]=='RevenueLoss'):
			dic["AverageCardholderSpent"]=la
			dic["AverageTransactionFrequency"]=lt
			dic["EcommerceSpent"]=ecom
			dic["NonEcommerceSpent"]=nonecom
	return dic
