var barChart = {};

barChart.draw = function(data) {
	var chart = bb.generate({
		title: {
			text: data.title
		},
		data: {
			json: data.json,
			names: data.names,
			type: 'bar',
			labels: false,
			colors: data.colors
		},
		legend: {
			usePoint: true
		},
		bar: {
			width: {
				ratio: 0.5
			}
		},
		zoom: {
			enabled: {
				type: 'drag'
			}
		},
		axis: {
			x: {
				label: {
					text: data.x_label,
					position: 'outer-center'
				}
			},
			y: {
				label: {
					text: data.y_label,
					position: 'outer-middle'
				}
			}
		},
		grid: {
			y: {
				lines: [
					{
						value: 0
					}
				]
			}
		},
		bindto: data.id
	});
}

