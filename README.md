# About the app

* The country is under a pandemic. Roads are closed, markets shut, people locked down. Government is struggling to keep its economy afloat. Chaos over food and resources drive people on the streets, making the situation even worse. Government needs to take urgent measures. But as with every organisation, they need statistics to make new policies.

* The more the small scale is affected, it paves more way for unemployment, poverty, obstructs opportunities, affects exports, rural and urban growth and overall sinks the economy on a bigger scale.

* So that's where Aggregator comes to solve these problems.

* Aggregator can be utilised to assess and monitor the economic situation of the varied levels starting from the substancial level to the bottommost one when a disaster hits.

* It provides insights into the real economic damage at various granularity levels and among various products/services and specific buying patterns which can help in identifying class of people or geographical locations experiencing significant economic loss due to a disaster.

# Prerequisites to run the app

* NodeJS
* Python
* Django
* MongoDB
* PyMongo

# How to run the app locally 

* Git clone the repository using `git clone` .
* Then move to the `Frontend` folder and in the terminal type `npm install` to install dependencies from `package.json` .

* After all the dependencies have been installed, type `nodemon app.js` in the terminal. The server will start running.

* Now, open another terminal in the `Backend` folder and type `python manage.py runserver` .
* Open `localhost:2000` in your browser and the website will be loaded.

# ScreenShots

![Home](/uploads/6015e82ddec8096d11f4b0bf09159919/Home.png)

![Country](/uploads/fcc548c3e212501f9bb567f3989c685c/Country.png)

![State](/uploads/28fc6b7287b77487492a3ccb0db3afb7/State.png)

![Pincode](/uploads/0d9455d5f2c14a310cf59968e114d78d/Pincode.png)

![MSA](/uploads/eb210654f5deda5dd2ad1952fae4b97d/MSA.png)

![Compare](/uploads/3ea4ed67747c4dd1f67266886ae61315/Compare.png)

# Preview of our analysis.

![Country_Graph](/uploads/d023af9bb51965054a4bc2da7f7974f3/Country_Graph.png)

![State_Graph](/uploads/f34450e6a4425a80ae7ce40a0bf5b4da/State_Graph.png)

![Pincode_Graph](/uploads/c154f110b3847f2534b61f06f4a63787/Pincode_Graph.png)

![MSA_Graph](/uploads/3f3815a7e59c331163ffe554c8aede29/MSA_Graph.png)

![Comparison_Graph](/uploads/91c10f5efc1de46c31e54b537a20f5ed/Comparison_Graph.png)
