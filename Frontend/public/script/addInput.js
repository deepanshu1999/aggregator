var counter = 2;
var limit = 4;

function addInput(divName) {
    if (counter == limit) {
        alert("You have reached the limit of adding " + counter + " inputs");
    } else {
        var newdiv = document.createElement('div');
        newdiv.innerHTML = `<input type='text' class='form-control mt-2' name='code${counter + 1}' placeholder='Area ${counter + 1}' autocomplete="off" required>`;
        document.getElementById(divName).appendChild(newdiv);
        counter++;
    }
}